﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Lab3.DAL.Entities;

namespace Lab3.Controllers
{
    [Produces("application/json")]
    [Route("api/ProductSubcategories")]
    public class ProductSubcategoriesController : Controller
    {
        private readonly AdventureWorks2014Context _context;

        public ProductSubcategoriesController(AdventureWorks2014Context context)
        {
            _context = context;
        }

        // GET: api/ProductSubcategories
        [HttpGet]
        public async Task<IActionResult> GetProductSubcategory()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subCategories = await _context.ProductSubcategory.ToListAsync();

            if (subCategories == null)
            {
                return NotFound();
            }

            return Ok(subCategories);
        }        
    }
}