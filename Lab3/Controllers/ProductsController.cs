﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Lab3.DAL.Entities;

namespace Lab3.Controllers
{
    [Produces("application/json")]    
    public class ProductsController : Controller
    {
        private readonly AdventureWorks2014Context _context;

        public ProductsController(AdventureWorks2014Context context)
        {
            _context = context;
        }

        // GET: api/Products
        [HttpGet("api/Products")]
        public async Task<IActionResult> GetList()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var products = await _context.Product.Include(x => x.ProductSubcategory).ToListAsync();

            if (products == null)
            {
                return NotFound();
            }

            return Ok(products);
        }

        // GET: api/Products/5

        [HttpGet("api/Products/{id}")]
        public async Task<IActionResult> Get(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var product = await _context.Product.SingleOrDefaultAsync(m => m.ProductId == id);

            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }

        [HttpGet("/api/Products/Filter/{id}/{query?}")]
        public IActionResult Filter([FromRoute] int id, [FromRoute] string query)
        {            
            IEnumerable<Product> products = _context.Product.Include(q => q.ProductSubcategory);

            if (id > 0)
                products = products.Where(x => x.ProductSubcategoryId == id);
            if (!string.IsNullOrEmpty(query))
                products = products.Where(x => x.Name.Contains(query));
            
            return Ok(products.ToList());
        }
    }
}