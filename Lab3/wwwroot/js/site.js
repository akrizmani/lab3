﻿var getProducts = function () {
    $.ajax({
        type: "GET",
        url: 'api/Products',
        error: function (error) {
            console.log(error);
        },
        success: function (response) {
            //console.log(response);

            $.each(response, function (i, product) {
                var productSubcategory = "";

                if (product.productSubcategory) {

                    productSubcategory = product.productSubcategory.name;
                }
                json_data = '<tr>' +
                    '<td>' + product.productId + '</td>' +
                    '<td>' + product.name + '</td>' +
                    '<td>' + productSubcategory + '</td>' +
                    '</tr>';
                $(json_data).appendTo('tbody');
            });

        }
    });
};

var fitlerProducts = function () {
    var name = $("#name").val();
    var subCategory = $('select').val();
    $.ajax({
        type: "GET",
        url: 'api/Products/Filter/' + subCategory + '/' + name,
        error: function (error) {
            console.log(error);
        },
        success: function (response) {
            //console.log(response);
            $('tbody').empty();
            $.each(response, function (i, product) {
                var productSubcategory = "";

                if (product.productSubcategory) {

                    productSubcategory = product.productSubcategory.name;
                }
                json_data = '<tr>' +
                    '<td>' + product.productId + '</td>' +
                    '<td>' + product.name + '</td>' +
                    '<td>' + productSubcategory + '</td>' +
                    '</tr>';
                $(json_data).appendTo('tbody');
            });

        }
    });
};

$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: 'api/ProductSubcategories',
        error: function (error) {
            console.log(error);
        },
        success: function (response) {
            //console.log(response);
            $.each(response, function (i, subCategory) {
                $('#subCategories').append($('<option>', {
                    value: subCategory.productSubcategoryId,
                    text: subCategory.name
                }));
            });
            getProducts();
        }
    });
});

$("#subCategories").change(function () {
    fitlerProducts();
});

$("#search").click(function () {
    fitlerProducts();
});

$("#name").on('keyup', function (e) {
    if (e.keyCode === 13) {
        fitlerProducts();
    }
});